package fr.uavignon.ceri.tp2.model;

import android.app.Application;

import java.util.List;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.repository.BookRepository;

public class ListViewModel extends AndroidViewModel {
    private static BookRepository rep;
    private BookRepository repository;
    private LiveData<List<Book>> allBooks;

    public ListViewModel (Application application) {
        super(application);
        repository = new BookRepository(application);
        allBooks = repository.getAllbooks();
    }
    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public static int getSize() {
        return rep.getSize();
    }

}