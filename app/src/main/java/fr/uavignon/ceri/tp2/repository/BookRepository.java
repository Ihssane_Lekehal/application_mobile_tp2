package fr.uavignon.ceri.tp2.repository;


import android.app.Application;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import fr.uavignon.ceri.tp2.DAO.BookDao;
import fr.uavignon.ceri.tp2.DAO.BookRoomDatabase;
import fr.uavignon.ceri.tp2.data.Book;

import static fr.uavignon.ceri.tp2.DAO.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {
    private MutableLiveData<List<Book>> selectedBook = new MutableLiveData<List<Book>>();
    private LiveData<List<Book>> allBooks;
    private BookDao bookDao;
    private BookRoomDatabase brdb;
    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();

    }
    public LiveData<List<Book>> getAllbooks() {
        return allBooks;
    }
    public MutableLiveData<List<Book>> getSelectedBook() {
        return selectedBook;
    }
    public void insertBook(Book newbook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(newbook);
        });
    }
    public void updateBook(Book book) {
        databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(book);
        });
    }
    public void deleteBook(Long id) {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(id);
        });
    }
    public void findBook(long id) {
        Future<List<Book>> fbooks = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);
        });
        try {
            selectedBook.setValue(fbooks.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getSize(){
        return bookDao.getCount();
    }
}