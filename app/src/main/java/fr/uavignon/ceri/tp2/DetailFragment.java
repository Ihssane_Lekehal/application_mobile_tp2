package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.IllegalFormatCodePointException;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.model.DetailViewModel;
import fr.uavignon.ceri.tp2.model.ListViewModel;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        DetailViewModel viewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        ListViewModel viewModel1 = new ViewModelProvider(this).get(ListViewModel.class);


        //.Book.books[(int)args.getBookNum();

        textTitle = (EditText) view.findViewById(R.id.nameBook);
        textAuthors = (EditText) view.findViewById(R.id.editAuthors);
        textYear = (EditText) view.findViewById(R.id.editYear);
        textGenres = (EditText) view.findViewById(R.id.editGenres);
        textPublisher = (EditText) view.findViewById(R.id.editPublisher);

        if ((int)args.getBookNum() > 1){
            MutableLiveData<List<Book>> book = viewModel.getBook((int)args.getBookNum());
            textTitle.setText(book.getValue().get(0).getTitle());
            textAuthors.setText(book.getValue().get(0).getAuthors());
            textYear.setText(book.getValue().get(0).getYear());
            textGenres.setText(book.getValue().get(0).getGenres());
            textPublisher.setText(book.getValue().get(0).getPublisher());
        }

        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
        view.findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!textTitle.getText().toString().isEmpty() && !textAuthors.getText().toString().isEmpty()){
                    Book book = new Book(
                            textTitle.getText().toString(), textAuthors.getText().toString(),
                            textYear.getText().toString(), textGenres.getText().toString(),
                            textPublisher.getText().toString());
                    book.setId((int)args.getBookNum());
                    try {
                        viewModel.insertOrUpdateBook(book);
                        Toast.makeText(view.getContext(), "MAJ effectuee", Toast.LENGTH_SHORT).show();
                    }catch (Exception e){
                        e.getStackTrace();
                    }
                }else{
                    Snackbar.make(view, "Merci de remplir tous les champs", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

            }
        });
    }
}