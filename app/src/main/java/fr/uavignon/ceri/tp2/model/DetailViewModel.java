package fr.uavignon.ceri.tp2.model;

import android.app.Application;

import java.util.List;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.repository.BookRepository;

public class DetailViewModel extends AndroidViewModel {

    private BookRepository repository;
    private LiveData<List<Book>> allBooks;

    public DetailViewModel (Application application) {
        super(application);
        repository = new BookRepository(application);
        allBooks = repository.getAllbooks();
    }

    public void insertBook(Book book) {
        repository.insertBook(book);
    }

    public void insertOrUpdateBook(Book book) {
        if (book != null && book.getId() != -1){
            repository.updateBook(book);}

        else{
            book.setId(0);
            insertBook(book);
        }

    }

    public void getBookById(long id) {
        repository.getSelectedBook();
    }

    public void deleteBook(long id) {
        repository.deleteBook(id);
    }


    public MutableLiveData<List<Book>> getBook(long id){
        repository.findBook(id);
        return repository.getSelectedBook();
    }
}
