package fr.uavignon.ceri.tp2;

import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.model.DetailViewModel;


public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.tp2.RecyclerAdapter.ViewHolder> {

    private static final String TAG = fr.uavignon.ceri.tp2.RecyclerAdapter.class.getSimpleName();
    private ActionMode mActionMode;
    public List<Book> books;
    private long bookId;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemTitle.setText(books.get(i).getTitle());
        viewHolder.itemDetail.setText(books.get(i).getAuthors());

    }

    @Override
    public int getItemCount() {
        if (books == null) {
            return 0;
        }
        else {
            return books.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDetail;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);



            int position = getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    long id = RecyclerAdapter.this.books.get((int)getAdapterPosition()).getId();

                   // int position = getAdapterPosition();
                    /* Snackbar.make(v, "Click detected on chapter " + (position+1),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                     */
                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setBookNum(id);
                    Navigation.findNavController(v).navigate(action);


                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener(){
                @Override
                public boolean onLongClick(View view) {
                    //Toast.makeText(view.getContext(), "supprimer", Toast.LENGTH_SHORT).show();
                    if(mActionMode != null){
                        return false;
                    }
                    bookId = RecyclerAdapter.this.books.get((int)getAdapterPosition()).getId();
                    mActionMode = itemView.startActionMode(new ActionMode.Callback() {
                        @Override
                        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                            actionMode.getMenuInflater().inflate(R.menu.context_menu, menu);
                            return true;
                        }

                        @Override
                        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                            return false;
                        }

                        @Override
                        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                            if(menuItem.getItemId() == R.id.del){
                                try {
                                    DetailViewModel viewModel = new ViewModelProvider((ViewModelStoreOwner) view.getContext()).get(DetailViewModel.class);
                                    viewModel.deleteBook(bookId);
                                    Toast.makeText(view.getContext(), "Suppression effectuee", Toast.LENGTH_SHORT).show();
                                    actionMode.finish();
                                }catch (Exception e){
                                    e.getStackTrace();
                                }
                                return true;
                            }
                            else{
                                return false;
                            }
                        }

                        @Override
                        public void onDestroyActionMode(ActionMode actionMode) {
                            mActionMode = null;
                        }
                    });
                    return true;
                }
            });

        }

    }
    public void setBookList(LiveData<List<Book>> books){
        this.books = books.getValue();
    }
    public void setBookList(List<Book> books){
        this.books = books;
    }

}