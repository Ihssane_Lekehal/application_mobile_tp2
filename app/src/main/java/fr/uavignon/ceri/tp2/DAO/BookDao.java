package fr.uavignon.ceri.tp2.DAO;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import fr.uavignon.ceri.tp2.data.Book;


@Dao
public interface BookDao {

    @Insert
    void insertBook(Book book);
    @Query("SELECT * FROM books WHERE bookId = :id")
    List<Book> getBook(Long id);
    @Query("DELETE FROM books WHERE bookId = :id")
    void deleteBook(Long id);
    @Query("DELETE FROM books")
    void deleteAllBooks();
    @Update
    void updateBook(Book book);
    @Query("SELECT * FROM books")
    LiveData<List<Book>> getAllBooks();


    @Query("SELECT COUNT(*) FROM books")
    int getCount();

}